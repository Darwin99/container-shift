package com.containershift;

import java.awt.PageAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

public class Platform {

	private ArrayList<Stack> firstTerminal;
	private ArrayList<Stack> secondTerminal;
	private int stacksNumber;
	private int stackSize;
	@SuppressWarnings("rawtypes")
	private HashMap containers;
	@SuppressWarnings("rawtypes")
	private HashMap futurPositions;

	@SuppressWarnings("rawtypes")
	public Platform(ArrayList<Stack> firstTerminal, ArrayList<Stack> secondTerminal, int stackNumber, int stackSize) {
		this.firstTerminal = firstTerminal;
		this.secondTerminal = secondTerminal;
		this.stacksNumber = stackNumber;
		this.stackSize = stackSize;
		this.containers = new HashMap();
		this.futurPositions = new HashMap<Position, Container>();

	}

	public ArrayList<Stack> getFirstTerminal() {
		return firstTerminal;
	}

	public void setFirstTerminal(ArrayList<Stack> firstTerminal) {
		this.firstTerminal = firstTerminal;
	}

	public ArrayList<Stack> getSecondTerminal() {
		return secondTerminal;
	}

	public void setSecondTerminal(ArrayList<Stack> secondTerminal) {
		this.secondTerminal = secondTerminal;
	}

	public int stackNumber() {
		return this.stacksNumber;
	}

	public int stackSize() {
		// TODO Auto-generated method stub
		return stackSize;
	}

	public boolean addStackToFistTerminal(Stack stack) {
		return firstTerminal.add(stack);
	}

	public boolean addStackToSecondTerminal(Stack stack) {
		return secondTerminal.add(stack);
	}

	@SuppressWarnings("unchecked")
	public HashMap<Integer, Container> containers() {
		return this.containers;
	}

	@SuppressWarnings("rawtypes")
	public HashMap futurPositions() {
		return this.futurPositions;
	}

	public void setFuturPositions(HashMap<Position, Container> futurPositions) {
		this.futurPositions = futurPositions;
	}

	/**
	 * The container that we want to put on the right place is on the top of its
	 * stack
	 * 
	 * @param c
	 * @throws MovingException
	 */

	public void moveToRigthtPlace(Container c) throws MovingException {
		Stack presentStack = c.getPosipres().getStack();

		// recovery of stack in which we have to put the container
		Stack futurStack = c.getPosifut().getStack();
		Stack rememberPositions1 = new Stack(2 * c.getPosifut().getStack().size(), null); // this allow to remember the
																							// position
																							// of all moved containers
																							// in the present /stack
		Stack rememberPositions2 = new Stack(2 * c.getPosifut().getStack().size(), null);

		// We have to verify first if the container is on top of its stack

		if (presentStack.tab().get(presentStack.top()) != c) {
			// moving of all container until the top is the container that we wan to put in
			// the right place

			while ((presentStack.tab().get(presentStack.top()) != c) && !presentStack.isEmpty()) {
				// all these container must be remember in the rememberPositions1
				Container topContainer = presentStack.tab().get(presentStack.top());
				this.move(topContainer, this.getPositions(topContainer, futurStack), rememberPositions1);

			}

		}

		// Here the container that we want to moved is on the top of its stack

		// Now, we have to verify if we can directly put the container
		// then we have to verify if the top of the future stack is available
		ArrayList<Position> positions = new ArrayList<Position>(); // the available position ********GetPosition****
		positions.add(c.getPosifut());
		if (futurStack.isFull()) {
			throw new MovingException("The stack in which we want to move a specific container is alredy ful");
		}
		if (c.getPosifut().getLine() == (futurStack.top() + 1)) {
			this.move(c, positions, rememberPositions1);
			// we have to say that this container is on right place
			c.getPosipres().setStack(c.getPosifut().getStack());
			c.getPosipres().setLine(c.getPosifut().getLine());
			/*
			 * after, this the remeberPosition only contain the container which is in the
			 * right place,then we don't need to retains it position
			 */
			rememberPositions1.pop();

		} else {
			while (((c.getPosifut().getLine() != (futurStack.top() + 1))) && !futurStack.isEmpty()) {
				Container currentContainerInFS = futurStack.tab().get(futurStack.top());

				this.move(currentContainerInFS, this.getPositions(currentContainerInFS, presentStack),
						rememberPositions2);
			}

			this.move(c, positions, rememberPositions1);

			c.getPosipres().setStack(c.getPosifut().getStack());
			c.getPosipres().setLine(c.getPosifut().getLine());
			rememberPositions1.pop();

			// we have then to rearrange the moved container

			if (!rememberPositions2.isEmpty()) {

//				positions.remove(0);
//				positions.add(new Position(presentStack, presentStack.top() +1));
				Container last = rememberPositions2.pop();
				last.getPosipres().getStack().pop();
				presentStack.push(last);
				last.setPosiprec(last.getPosipres());
				last.setPosipres(new Position(presentStack, presentStack.top()));
//				this.move(rememberPositions2.pop(), positions, rememberPositions1);
//				rememberPositions1.pop();
				this.show();

			}
//			this.show();
			rememberPositions2 = rememberPositions2.reverse();

			while (!rememberPositions2.isEmpty()) {
				Container movedContainer = rememberPositions2.pop();
				movedContainer.getPosipres().getStack().pop();

				futurStack.push(movedContainer);
				Position position = new Position(futurStack, futurStack.top());
				Position position1 = movedContainer.getPosipres();
				movedContainer.setPosiprec(position1);
				movedContainer.setPosipres(position);
				this.show();
			}
		}

		/*************************************************************************************************/
		// Rearrange of containers that was moved in the present stack

		rememberPositions1 = rememberPositions1.reverse();

		if (!rememberPositions1.isEmpty()) {
			while (!rememberPositions1.isEmpty()) {
				Container movedContainer = rememberPositions1.pop();
				movedContainer.getPosipres().getStack().pop();

				presentStack.push(movedContainer);
				Position position = new Position(presentStack, presentStack.top());
				Position position1 = movedContainer.getPosipres();
				movedContainer.setPosiprec(position1);
				movedContainer.setPosipres(position);
				this.show();
			}
		}

		// Now, we are that the container is on it right place, the it present position
		// have to be the future position

		c.getPosipres().setStack(c.getPosifut().getStack());
		c.getPosipres().setLine(c.getPosifut().getLine());

	}

	/**
	 * --show-- show the state of the platform
	 * 
	 * @param void
	 * @return void
	 */
	public void show() {
		for (int i = (this.stackSize() - 1); i >= 0; i--) {

			// displaying of containers in the first terminal at the line i
			for (int j = 0; j < (this.stackNumber() / 2); j++) {
				Stack currentStack = this.getFirstTerminal().get(j);

				if (i <= currentStack.top()) {
					Container currentContainer = currentStack.tab().get(i);
					currentContainer.show();

				} else {
					System.out.print(" - ");
				}

			}
			System.out.print(" | ");
			// displaying of containers of the second terminal at the line i
			for (int j = 0; j < (this.stackNumber() / 2); j++) {
				Stack currentStack = this.getSecondTerminal().get(j);
				if (i <= currentStack.top()) {
					Container currentContainer = currentStack.tab().get(i);
					currentContainer.show();

				} else {
					System.out.print(" - ");
				}
			}
			System.out.println("\n");

		}

		System.out.println("----------------------------------------------------------------");

	}

	/**
	 * --move--move a specific container to the top of a given stack
	 * 
	 * @param Container           c the container is on top of it stack
	 * @param ArrayList<Position> in the first time, we will put the container at
	 *                            the first position
	 * @param Stack               rememberPositions which retain the positions of
	 *                            moved containers
	 * @return boolean
	 * @throws MovingException
	 */

	public boolean move(Container container, ArrayList<Position> positions, Stack rememberPositions)
			throws MovingException {

		
		if(positions.isEmpty())
			throw new MovingException("XXXXXXXXXXXXXXX  ERROR! XXXXXXXXXXXXXXX \nThere is no place to place A specfic container ");
		Container c1 = container.getPosipres().getStack().pop();
//		System.out.println("FINISH");
//		
//		System.out.println("15 stack ");
//		container.getPosipres().getStack().show();
//		container.getPosipres().getStack().pop();
//		this.show();
//		 System.out.print(" -> ");
//		 container.getPosipres().getStack().show();
//		 container.getPosipres().getStack().pop();
//		 System.out.print(" -> ");
//		 container.getPosipres().getStack().show();
		

		int i = 0;

		while (positions.get(i).getStack().isFull() && i < positions.size()) {
			i++;
		}
		if (positions.size() == i) {
			throw new MovingException(
					"XXXXXXXXXXXXXXX  ERROR! XXXXXXXXXXXXXXX \nThere is no place to place A specfic container ");
		} else {
			if (positions.get(i).getStack().push(container)) {

				Position position = new Position(positions.get(0).getStack(), positions.get(0).getStack().top());
				container.setPosiprec(container.getPosipres());

				container.setPosipres(position);
				// registration of moved container
				rememberPositions.push(container);

				this.show();
				return true;
			}
		}

		throw new MovingException("XXXXXXXXXXXXXXX  ERROR! XXXXXXXXXXXXXXX \nThere is no place to place A specfic container ");

	}

	/**
	 * 
	 * @param c
	 * @param Stack removeStack The stack in which we won't put something because it
	 *              is the stack in which the container must be quit and it have to
	 *              be on top
	 * @return {@link ArrayList}
	 */

	public ArrayList<Position> getPositions(Container c, Stack removeStack) {

		ArrayList<Position> availablePositions = new ArrayList<Position>();
		for (int i = 0; i < this.stackNumber() / 2; i++) {
			Stack currentStack = this.getFirstTerminal().get(i);
			Boolean condition;
			if (removeStack == null) {
				condition = !currentStack.isFull() && (currentStack != c.getPosipres().getStack());

			} else {
				condition = (!currentStack.isFull()) && (currentStack != c.getPosipres().getStack())
						&& (currentStack != removeStack);

			}
			if (condition) {
				Position availablePosition = new Position(currentStack, currentStack.top() + 1);
				availablePositions.add(availablePosition);
			}
		}

		for (int i = 0; i < this.stackNumber() / 2; i++) {
			Stack currentStack = this.getSecondTerminal().get(i);
			if (!currentStack.isFull()) {
				Position availablePosition = new Position(currentStack, currentStack.top() + 1);
				availablePositions.add(availablePosition);
			}
		}
		return availablePositions;

	}

	public void goToFinalState() throws MovingException {
		System.out.println("*******************GO TO FINAL STATE********************************");
		System.out.println("\n");
		System.out.println("*******************First Terminal***********************************");
		for (int i = 0; i < this.stackNumber() / 2; i++) {
			System.out.println("################# Stack " + i + "  ##################");
			for (int j = 0; j < this.stackSize; j++) {
				// creation of the position where we are
				System.out.println("\n");
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%  CONTAINER " + j + "  %%%%%%%%%%%%%%%%%%%%%%%%%%%");
				System.out.println("\n");
				Position currentPosition = new Position(this.getFirstTerminal().get(i), j);
				Container searchedContainer = this.search(currentPosition);
				if (searchedContainer != null) {
					System.out.println("######################## Move container  TO FINAL PLACE   ############");
					this.moveToRigthtPlace(searchedContainer);

					System.out.println("\n##########################################################################");
				}
			}
		}

		System.out.println("\n");
		System.out.println("*******************Second Terminal***********************************");
		for (int i = 0; i < this.stackNumber() / 2; i++) {
			System.out.println("################# Stack " + i + "  ##################");
			for (int j = 0; j < this.stackSize; j++) {
				// creation of the position where we are
				System.out.println("\n");
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%  CONTAINER " + j + "  %%%%%%%%%%%%%%%%%%%%%%%%%%%");
				System.out.println("\n");
				Position currentPosition = new Position(this.getSecondTerminal().get(i), j);

				Container searchedContainer = this.search(currentPosition);
				if (searchedContainer != null) {

					this.moveToRigthtPlace(searchedContainer);
				}
			}
		}

	}

	public void configPlatform() {
		setFirstTerminal(init(this.stackNumber() / 2, "    CONFIGURATION DU TERMINAL A ", "A"));
		setSecondTerminal(init(this.stackNumber() / 2, "   CONFIGURATION DU TERMINAL B ", "B"));
	}

	public void initPlatform() {
		setFirstTerminal(init(this.stackNumber() / 2, "    CONFIGURATION DU TERMINAL A ", "A"));
		setSecondTerminal(init(this.stackNumber() / 2, "   CONFIGURATION DU TERMINAL B ", "B"));

		System.out.println("\n########### Votre platforme est bien initialis�e #################");
	}

	public void initFinalPlatform(Platform initial) {
		setFirstTerminal(initFinal(this.stackNumber() / 2, "    CONFIGURATION DU TERMINAL A ", "A", initial));
		setSecondTerminal(initFinal(this.stackNumber() / 2, "   CONFIGURATION DU TERMINAL B ", "B", initial));

		System.out.println("\n########### Votre platforme est bien initialis�e #################");
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Stack> init(int n, String message, String letter) {
		System.out.println("******************************************************************");
		System.out.println(message);
		System.out.println("******************************************************************");

		Scanner sc = new Scanner(System.in);
		ArrayList<Stack> stacktab = new ArrayList<Stack>();

		for (int i = 0; i < n; i++) {
			System.out.println("\n===> Entrer les �lements de la rang�e de containneur " + (i + 1));

			Stack rang = null;

			if (letter.equals("A"))
				rang = new Stack(n, firstTerminal);
			if (letter.equals("B"))
				rang = new Stack(n, secondTerminal);

			int line = 0;

			String response = "";

			do {
				System.out.println("\nEntrer le poids du container " + (line + 1));
				int val = sc.nextInt();
				sc.nextLine();

				Container c = new Container(val, new Position(rang, line));
				this.containers.put(c.getVal(), c);

				rang.push(c);

				line++;

				System.out.println("Taper o pour ajouter un nouveau container ou n pour passer � la crang�e suivante");
				response = sc.nextLine();

			} while (response.equalsIgnoreCase("o") && line < this.stackSize);

			stacktab.add(rang);
		}

		return stacktab;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Stack> initFinal(int n, String message, String letter, Platform initial) {
		System.out.println("******************************************************************");
		System.out.println(message);
		System.out.println("******************************************************************");

		Scanner sc = new Scanner(System.in);
		ArrayList<Stack> stacktab = new ArrayList<Stack>();

		for (int i = 0; i < n; i++) {
			System.out.println("\n===> Entrer les �lements de la rang�e de containneur " + (i + 1));

			Stack rang = null;

			if (letter.equals("A"))
				rang = new Stack(n, firstTerminal);
			if (letter.equals("B"))
				rang = new Stack(n, secondTerminal);

			int line = 0;

			String response = "";

			do {
				System.out.println("\nEntrer le poids du container " + (line + 1));
				int val = sc.nextInt();
				sc.nextLine();

				Container c = new Container(val, new Position(rang, line));

				rang.push(c);

				if (this.containers.containsKey(c.getVal())) {
					Container c1 = (Container) initial.containers().get(c.getVal());
					// now we have to modify the future position of the container c1 which is the
					// container in the initial platefform
					Position c1FuturPosition = new Position(rang, line);
					c1.setPosifut(c1FuturPosition);
					this.futurPositions.put(c1.getPosifut(), c1);
				}

				line++;

				System.out.println("Taper o pour ajouter un nouveau container ou n pour passer � la crang�e suivante");
				response = sc.nextLine();

			} while (response.equalsIgnoreCase("o") && line < this.stackSize);

			stacktab.add(rang);
		}

		return stacktab;
	}

	public Container search(Position futurPosition) {
		for (int i = 0; i < this.stacksNumber / 2; i++) {
			for (int j = 0; j <= this.getFirstTerminal().get(i).top(); j++) {
				Container cont = this.getFirstTerminal().get(i).tab().get(j);
				if ((cont.getPosifut().getStack() == futurPosition.getStack())
						&& (cont.getPosifut().getLine() == futurPosition.getLine())) {
					return cont;
				}
			}
		}

		for (int i = 0; i < this.stacksNumber / 2; i++) {
			for (int j = 0; j <= this.getSecondTerminal().get(i).top(); j++) {
				Container cont = this.getSecondTerminal().get(i).tab().get(j);
				if ((cont.getPosifut().getStack() == futurPosition.getStack())
						&& (cont.getPosifut().getLine() == futurPosition.getLine())) {
					return cont;
				}
			}
		}

		return null;
	}

}
