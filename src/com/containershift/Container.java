package com.containershift;

public class Container {
	private int val;
	private Position posiprec;
	private Position posipres;
	private Position posifut;
	
	public Container(int val, Position posipres) {
		this.val = val;
		this.posipres = posipres;
	}

	public int getVal() {
		return val;
	}

	public void setVal(int val) {
		this.val = val;
	}

	public Position getPosiprec() {
		return posiprec;
	}

	public void setPosiprec(Position posiprec) {
		this.posiprec = posiprec;
	}

	public Position getPosipres() {
		return posipres;
	}

	public void setPosipres(Position posipres) {
		this.posipres = posipres;
	}

	public Position getPosifut() {
		return posifut;
	}

	public void setPosifut(Position posifut) {
		this.posifut = posifut;
	}
	
	public void show()
	{
		System.out.print(" " + this.getVal() + " ");
	}
	
//	public void displayContainer() {
//		return ("(val:"+this.val+";pospres:"+this.posipres)
//	}
}
