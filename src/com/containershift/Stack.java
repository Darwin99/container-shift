package com.containershift;

import java.util.ArrayList;

public class Stack {

	private int top;
	private int size;
	private ArrayList<Container> tab;
	private ArrayList<Stack> terminal;
	
	
	public Stack(int size,ArrayList<Stack> terminal)
	{
		this.top =-1;
		this.size = size;
		this.tab = new ArrayList<Container>(this.size);
		this.setTerminal(terminal);
	}
	
	public int size()
	{
		return this.size;
	}
	
	
	public int top()
	{
		return top;
	}
	
	public ArrayList<Container> tab()
	{
		return tab;
	}

	public boolean isFull() 
	{
		if ((this.size-1) == this.top)
			return true; // the top of the stack is the element at the position size of the table
		return false;
	}

	public boolean isEmpty()
	{
		if (this.top == -1)
			return true;
		return false;
	}

	public boolean push(Container c) {

		if (!isFull()) {
			tab.add(c);
			this.top++;
			return true;
		}

		return false;
	}

	public Container pop()
	{
		if (!isEmpty()) {
			Container c = tab.get(top);
			tab.remove(c);
			
			this.top--;
			
			return c;
			
		}
		return null;
	}

	public ArrayList<Stack> getTerminal()
	{
		return terminal;
	}

	public void setTerminal(ArrayList<Stack> terminal) 
	{
		this.terminal = terminal;
	}
	
	public void displayStack() 
	{
		
	}
	
	public void  removeAll()
	{
		this.tab.clear();
		this.top = -1;
	}
	
	public Stack reverse()
	{
		Stack reverse = new Stack(this.size,this.terminal);
		for(int i=this.top;i >=0 ;i--)
		{
			reverse.push(this.tab.get(i));
		}
		 return reverse;
	}
	
	public void show()
	{
		for(int i=top;i >= 0;i--)
		{
			tab.get(i).show();
			System.out.println("\n");
		}
	}

}
