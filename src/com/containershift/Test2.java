
package com.containershift;

import java.util.ArrayList;
import java.util.HashMap;

public class Test2 {

	public static void main(String[] args) throws MovingException {

		
		ArrayList<Stack> terminalA = new ArrayList<Stack>();
		ArrayList<Stack> terminalB = new ArrayList<Stack>();

		// STACK 1
		Stack s1 = new Stack(6, terminalA);
		Container c10 = new Container(19, new Position(s1, 0));
		Container c11 = new Container(13, new Position(s1, 1));
		Container c12 = new Container(7, new Position(s1, 2));

		s1.push(c10);
		s1.push(c11);
		s1.push(c12);
		
		// STACK 2

		Stack s2 = new Stack(6, terminalA);
		Container c20 = new Container(18, new Position(s2, 0));
		Container c21 = new Container(12, new Position(s2, 1));
		Container c22 = new Container(6, new Position(s2, 2));

		s2.push(c20);
		s2.push(c21);
		s2.push(c22);
		
		// STACK 3

		Stack s3 = new Stack(6, terminalA);
		Container c30 = new Container(17, new Position(s3, 0));
		Container c31 = new Container(11, new Position(s3, 1));
		Container c32 = new Container(5, new Position(s3, 2));

		s3.push(c30);
		s3.push(c31);
		s3.push(c32);
		
		// TERMINAL B

		// STACK 1
		Stack s4 = new Stack(6, terminalB);
		Container c40 = new Container(16, new Position(s4, 0));
		Container c41 = new Container(10, new Position(s4, 1));
	

		s4.push(c40);
		s4.push(c41);
		
		// STACK 2

		Stack s5 = new Stack(6, terminalB);
		Container c50 = new Container(15, new Position(s5, 0));
		Container c51 = new Container(9, new Position(s5, 1));
		

		s5.push(c50);
		s5.push(c51);
		

		// STACK 3

		Stack s6 = new Stack(6, terminalB);
		Container c60 = new Container(14, new Position(s6, 0));
		Container c61 = new Container(8, new Position(s6, 1));
	

		s6.push(c60);
		s6.push(c61);
		
	
		// ADD STACK TO TERMINAL
		
		terminalA.add(s1);
		terminalA.add(s2);
		terminalA.add(s3);

		terminalB.add(s4);
		terminalB.add(s5);
		terminalB.add(s6);

		Platform initial = new Platform(terminalA, terminalB, 6,6);
		initial.show();

		
		
		//########################################################################################################################################
		
		ArrayList<Stack> terminalAFut = new ArrayList<Stack>();
		ArrayList<Stack> terminalBFut = new ArrayList<Stack>();

		// STACK 1
		Stack s1Fut = new Stack(6, terminalAFut);
		Container c10Fut = new Container(5, new Position(s1, 0));
		Container c11Fut = new Container(11, new Position(s1, 1));
		Container c12Fut = new Container(17, new Position(s1, 2));

		s1Fut.push(c10Fut);
		s1Fut.push(c11Fut);
		s1Fut.push(c12Fut);
		
		// STACK 2

		Stack s2Fut = new Stack(6, terminalAFut);
		Container c20Fut = new Container(6, new Position(s2, 0));
		Container c21Fut = new Container(12, new Position(s2, 1));
		Container C22Fut = new Container(18, new Position(s2, 2));

		s2Fut.push(c20Fut);
		s2Fut.push(c21Fut);
		s2Fut.push(C22Fut);
		

		// STACK 3

		Stack s3Fut = new Stack(6, terminalAFut);
		Container c30Fut = new Container(7, new Position(s3, 0));
		Container c31Fut = new Container(13, new Position(s3, 1));
		Container c32Fut = new Container(19, new Position(s3, 2));

		s3Fut.push(c30Fut);
		s3Fut.push(c31Fut);
		s3Fut.push(c32Fut);
		

		// TERMINAL B

		// STACK 1
		Stack s4Fut = new Stack(6, terminalBFut);
		Container c40Fut = new Container(8, new Position(s4, 0));
		Container c41Fut = new Container(14, new Position(s4, 1));
	

		s4Fut.push(c40Fut);
		s4Fut.push(c41Fut);
		
		// STACK 2

		Stack s5Fut = new Stack(6, terminalBFut);
		Container c50Fut = new Container(9, new Position(s5, 0));
		Container c51Fut = new Container(15, new Position(s5, 1));
		

		s5Fut.push(c50Fut);
		s5Fut.push(c51Fut);
		
		// STACK 3

		Stack s6Fut = new Stack(6, terminalBFut);
		Container c60Fut = new Container(10, new Position(s6, 0));
		Container c61Fut = new Container(16, new Position(s6, 1));
	

		s6Fut.push(c60Fut);
		s6Fut.push(c61Fut);
		
	
		// ADD STACK TO TERMINAL
		
		terminalAFut.add(s1Fut);
		terminalAFut.add(s2Fut);
		terminalAFut.add(s3Fut);

		terminalBFut.add(s4Fut);
		terminalBFut.add(s5Fut);
		terminalBFut.add(s6Fut);

		Platform finalPlateform = new Platform(terminalAFut, terminalBFut, 6, 6);
		finalPlateform.show();
		
		c10.setPosifut(c32Fut.getPosipres());
		c11.setPosifut(c31Fut.getPosipres());
		c12.setPosifut(c30Fut.getPosipres());
		c20.setPosifut(C22Fut.getPosipres());
		c21.setPosifut(c21Fut.getPosipres());
		c22.setPosifut(c20Fut.getPosipres());
		c30.setPosifut(c12Fut.getPosipres());
		c31.setPosifut(c11Fut.getPosipres());
		c32.setPosifut(c10Fut.getPosipres());
		c40.setPosifut(c61Fut.getPosipres());
		c41.setPosifut(c60Fut.getPosipres());
		c50.setPosifut(c51Fut.getPosipres());
		c51.setPosifut(c50Fut.getPosipres());
		c60.setPosifut(c41Fut.getPosipres());
		c61.setPosifut(c40Fut.getPosipres());
		
		System.out.println("\n");
		
		initial.goToFinalState();
		
		
		
		
	}

}
