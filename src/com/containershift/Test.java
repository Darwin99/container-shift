

package com.containershift;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
public class Test { 
	
	public static void main(String[] args) throws MovingException { 	
		// TODO Auto-generated method stub 				
//		Scanner sc = new Scanner(System.in); 		
//		Stack stack = new Stack(3,new ArrayList<Stack>()); 			
//		Platform initial = new Platform(null,null,4,2); 	
//		initial.initPlatform(); 
//		initial.show();
//		System.out.println("**********************************************************************************");
//		System.out.println("                     ENTRY OF FINAL PLATEFORM STATE                               ");
//		System.out.println();
//		System.out.println("**********************************************************************************");
//		System.out.println();
//		
//		Platform finalState = new Platform(null, null, 4, 2);
//		finalState.initFinalPlatform(initial);
//		finalState.show();
//		
//		initial.goToFinalState();
//		initial.show();
		HashMap<Integer, Container> containers = new HashMap<Integer, Container>();
		HashMap<Position, Container> futurPosition = new HashMap<Position, Container>();
		
		ArrayList<Stack> terminalA = new ArrayList<Stack>();
		ArrayList<Stack> terminalB = new ArrayList<Stack>();
		
		Stack s1 =new Stack(6, terminalA);
			Container c10 = new Container(2, new Position(s1, 0));
			Container c11 = new Container(9, new Position(s1, 1));
			Container c12 = new Container(10, new Position(s1, 2));

		s1.push(c10);
		s1.push(c11);
		s1.push(c12);
		
		
		Stack s2 =new Stack(6, terminalA);
			Container c20 = new Container(1, new Position(s2, 0));
			s2.push(c20);
		
		Stack s3 = new Stack(6,terminalA);
				Container c30 = new Container(3, new Position(s3, 0));
			s3.push(c30);
			
			
			
		
			
			
		
		Stack s4 =new Stack(6, terminalB);
			Container c40= new Container(7, new Position(s4, 0));
		s4.push(c40);
		
		
		Stack s5 =new Stack(6, terminalB);
			Container c50 = new Container(8, new Position(s5, 0));
			Container c51 = new Container(6, new Position(s5, 1));
			Container c52 = new Container(5, new Position(s5, 2));
		s5.push(c50);
		s5.push(c51);
		s5.push(c52);
		
		
		
		 Stack s6 = new Stack(6,terminalB);
		 	Container c60 = new Container(15, new Position(s6,0));
		 	Container c61 = new Container(16, new Position(s6,1));
		 	Container c62 = new Container(4, new Position(s6,2));
		 s6.push(c60);
		 s6.push(c61);
		 s6.push(c62);
		
		terminalA.add(s1);
		terminalA.add(s2);
		terminalA.add(s3);
		
		
		
		terminalB.add(s4);
		terminalB.add(s5);
		terminalB.add(s6);
		
		Platform initial = new Platform(terminalA, terminalB, 6, 6);
//		initial.show();
		
		
		
		
		ArrayList<Stack> terminalAFut = new ArrayList<Stack>();
		ArrayList<Stack> terminalBFut = new ArrayList<Stack>();
		
		Stack s1Fut =new Stack(6, terminalAFut);
			Container c10Fut = new Container(1, new Position(s1, 0));
			Container c11Fut = new Container(7, new Position(s1, 1));
		s1Fut.push(c10Fut);
		s1Fut.push(c11Fut);
		
		Stack s2Fut =new Stack(6, terminalAFut);
			Container c20Fut = new Container(2, new Position(s2, 0));
			Container c21Fut = new Container(8, new Position(s2, 1));
			
		s2Fut.push(c20Fut);
		s2Fut.push(c21Fut);
		
		Stack s3Fut =new Stack(6, terminalAFut);
			Container c30Fut= new Container(3, new Position(s3, 0));
			Container c31Fut = new Container(9, new Position(s3, 1));
		s3Fut.push(c30Fut);
		s3Fut.push(c31Fut);
		
		terminalAFut.add(s1Fut);
		terminalAFut.add(s2Fut);
		terminalAFut.add(s3Fut);

		
		Stack s4Fut =new Stack(6, terminalBFut);
			
			Container c40Fut = new Container(4, new Position(s4, 0));
			Container c41Fut = new Container(10, new Position(s4, 1));
			
		s4Fut.push(c40Fut);
		s4Fut.push(c41Fut);
		
		
		Stack s5Fut =new Stack(6, terminalBFut);
		
			Container c50Fut = new Container(5, new Position(s5, 0));
			Container c51Fut = new Container(15, new Position(s5, 1));
		
		s5Fut.push(c50Fut);
		s5Fut.push(c51Fut);
		
		Stack s6Fut =new Stack(6, terminalBFut);
		
			Container c60Fut = new Container(6, new Position(s6, 0));
			Container c61Fut = new Container(16, new Position(s6, 1));
		
		s6Fut.push(c60Fut);
		s6Fut.push(c61Fut);
		
		
		
		
		terminalBFut.add(s4Fut);
		terminalBFut.add(s5Fut);
		terminalBFut.add(s6Fut);
		
		
		
		Platform final1 = new Platform(terminalAFut, terminalBFut, 6,6);
//		final1.show();
		
		c10.setPosifut(c20Fut.getPosipres());
		c11.setPosifut(c31Fut.getPosipres());
		c12.setPosifut(c41Fut.getPosipres());
		c20.setPosifut(c10Fut.getPosipres());
		c30.setPosifut(c30Fut.getPosipres());
		c40.setPosifut(c11Fut.getPosipres());
		c50.setPosifut(c21Fut.getPosipres());
		c51.setPosifut(c60Fut.getPosipres());
		c52.setPosifut(c50Fut.getPosipres());
		c60.setPosifut(c51Fut.getPosipres());
		c61.setPosifut(c61Fut.getPosipres());
		c62.setPosifut(c40Fut.getPosipres());
		
		initial.show();
		
		System.out.println("\n");
		final1.show();
		System.out.println();
		
		initial.goToFinalState();
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		initial.show();
		
//		futurPosition.put(c21.getPosipres(),c1);
//		
//		c2.setPosifut(c61.getPosipres());
//		futurPosition.put(c61.getPosipres(),c2);
//		
//		c3.setPosifut(c11.getPosipres());
//		futurPosition.put(c11.getPosipres(),c3);
//		
//		c5.setPosifut(c31.getPosipres());
//		futurPosition.put(c31.getPosipres(),c5);
//		
//		c6.setPosifut(c51.getPosipres());
//		
//		futurPosition.put(c51.getPosipres(),c6);
//		
//		c7.setPosifut(c71.getPosipres());
//		futurPosition.put(c71.getPosipres(),c7);
//		
//		initial.setFuturPositions(futurPosition);
//		
//		
//		
////		initial.move(c5,initial.getPositions(c5,s2),new Stack(15,null));
////		initial.moveToRigthtPlace(c5);
////		initial.goToFinalState();
//		initial.show();
//		
//		
//		initial.goToFinalState();
//		initial.show();
//	initial.getPositions(c1);
//		initial.moveToRigthtPlace(c3);
//		initial.moveToRigthtPlace(c5);
//		c3.getPosifut().getStack().show();
			
		
		
		
//		ArrayList<Integer>  a = new ArrayList<Integer>(2);
//		a.add(1);
//		a.add(2);
//		
//		System.out.println(a.get(2));
//		
	} 
	

}

