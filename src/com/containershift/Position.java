package com.containershift;

public class Position {
	
	private Stack stack;
	private int line;
	
	
	public Position(Stack stack, int line) {
		this.stack = stack;
		this.line = line;
	}

//	public Position(Position posi) {
//		// TODO Auto-generated constructor stub
//		
//		this.stack = new Stack(posi.getStack());
//		this.line = posi.getLine();
//	}

	public Stack getStack() {
		return stack;
	}

	public void setStack(Stack stack) {
		this.stack = stack;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	
	public  boolean equals(Position p)
	{
		if((this.getStack()==p.getStack()) && (this.getLine()==p.getLine()))
			return true;
		return false;
	}
	
	
	
}
